package com.example.pavlo.taskproject;

import android.content.Context;
import android.widget.Toast;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
/**
 * Created by Pasha on 18.12.2015.
 */
public class TaskManager {
    private static final String TAG = "TaskManager";
    private static final String FILENAME = "tasks.json";

    private ArrayList<Task> mTaskArrayList;
    private TaskJSONSerializer mTaskJSONSerializer;
    private static TaskManager sTaskManager;
    private Context mContext;

    public TaskManager(ArrayList<Task> taskArrayList, Context context) {
        mTaskArrayList = taskArrayList;
        mContext = context;
    }
    public void addTask(Task task){
        mTaskArrayList.add(task);
    }

    public boolean saveTask(){

        try {
            mTaskJSONSerializer.saveTask(mTaskArrayList);
            return true;
        } catch (JSONException e) {
            Toast.makeText(mContext, "JSON Exception", Toast.LENGTH_LONG).show();
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            Toast.makeText(mContext, "IOExecption", Toast.LENGTH_LONG).show();
            e.printStackTrace();
            return false;
        }
    }
}
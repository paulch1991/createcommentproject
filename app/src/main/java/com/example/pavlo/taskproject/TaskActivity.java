package com.example.pavlo.taskproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by Pasha on 27.11.2015.
 */

public class TaskActivity extends AppCompatActivity implements Constants{

    private Button mButtonSaveTask, mButtonCancel;
    private TextView mTextViewTitleTask, mTextViewDescription;
    private EditText mEditTextTitleTask, mEditTextDescription;
    private Task mTask;
    int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);
        initUI();

        Intent intent = getIntent();
        Log.i(KEY_LONG_CLICK_ITEM, String.valueOf(intent.getParcelableExtra(KEY_LONG_CLICK_ITEM)));
        //Log.i(KEY_POSITION_ITEM, String.valueOf(intent.getParcelableExtra(KEY_POSITION_ITEM)));
        if(intent.hasExtra(KEY_LONG_CLICK_ITEM)){
            mTask = intent.getParcelableExtra(KEY_LONG_CLICK_ITEM);
            position = intent.getIntExtra(KEY_POSITION_ITEM, DEFAULT_KEYS_DIALER);
            mEditTextTitleTask.setText(mTask.getStringTitle());
            mEditTextDescription.setText(mTask.getStringDescription());
        }

        setUI();

    }

    @Override
    public void initUI(){
        //initialize all object in our activity_task.xml
        mTextViewTitleTask = (TextView) findViewById(R.id.tvTitleTask);
        mTextViewDescription = (TextView) findViewById(R.id.tvDescription);

        mEditTextTitleTask = (EditText) findViewById(R.id.editTextTitleTask);
        mEditTextDescription = (EditText) findViewById(R.id.editTextDescription);
        //create object Task, there are two String.

        //initialize button_save and and bottom_task_cancel
        mButtonSaveTask = (Button) findViewById(R.id.buttonSaveTask);
        mButtonCancel = (Button) findViewById(R.id.buttonTaskCancel);
//        mEditTextTitleTask.setText(mTask.getStringTitle());
//        mEditTextDescription.setText(mTask.getStringDescription());
    }

    @Override
    public void setUI(){

        mButtonSaveTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //create object Task, there are two String.
                mTask = new Task(mEditTextTitleTask.getText().toString(),
                        mEditTextDescription.getText().toString());

                if (mTask != null) {
                    Intent intent = new Intent();
                    intent.putExtra(KEY_PARCELABLE_EXTRA, mTask);
                    intent.putExtra(KEY_POSITION_ITEM_BACK, position);
                    setResult(RESULT_OK, intent);
                    finish();
                }
            }
        });

        //initialize button_cancel and close this activity.
        mButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}

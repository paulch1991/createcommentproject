package com.example.pavlo.taskproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.example.pavlo.taskproject.adapter.TaskAdapter;

import org.json.JSONException;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;

public class MainActivity extends AppCompatActivity implements Constants {

    private Button mButtonAddTask;
    private TaskAdapter mTaskAdapter;
    private ArrayList<Task> mTaskArrayList;
    private Task mTask;
    private ListView listView;
    private SimpleDateFormat mSimpleDateFormat, mDifferenceTime;
    private Intent intent;
    private long dateTimeStart, datetimeEnd;
    //private TaskManager mTaskManager;
    private TaskJSONSerializer mTaskJSONSerializer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState != null) {
            mTaskArrayList = savedInstanceState.getParcelableArrayList(KEY_SAVE_INSTANCE);
        } else {
            mTaskArrayList = new ArrayList<>();
        }
        initUI();
        setUI();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i(KEY_POSITION_ITEM_BACK, String.valueOf(requestCode));

        //
        if(data != null){
            switch (requestCode) {
                case 1:
                    mTask = data.getParcelableExtra(KEY_PARCELABLE_EXTRA);
                    mTaskArrayList.add(mTask);
                    Log.i("result code in 1", String.valueOf(resultCode));
                    initUI();
                    break;
                case 2:
                    int position = data.getIntExtra(KEY_POSITION_ITEM_BACK, DEFAULT_KEYS_DIALER);
                    mTask = data.getParcelableExtra(KEY_PARCELABLE_EXTRA);

                    mTaskArrayList.set(position, mTask);

                    Log.i("result code in 2", String.valueOf(resultCode));
                    initUI();
                    break;
                default:
                    Log.i("result code in 3", String.valueOf(resultCode));
                    break;
            }
        }
        initUI();


    }


    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putParcelableArrayList(KEY_SAVE_INSTANCE, mTaskArrayList);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mTaskArrayList = savedInstanceState.getParcelableArrayList(KEY_SAVE_INSTANCE);
    }

    @Override
    public void initUI() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        mSimpleDateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        mDifferenceTime = new SimpleDateFormat("HH:mm:ss");

        mButtonAddTask = (Button) findViewById(R.id.buttonAddTask);
        mTaskAdapter = new TaskAdapter(this, mTaskArrayList);
        listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(mTaskAdapter);
    }

    @Override
    public void setUI() {

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String formatDateTime = mSimpleDateFormat.format(GregorianCalendar.getInstance().getTimeInMillis());
                //String formatDateTime = mSimpleDateFormat.format(System.currentTimeMillis());

                Task task = mTaskAdapter.getItem(position);



                if (task.getStartDateTask() == null) {
                    task.setStartDateTask(formatDateTime);
                    dateTimeStart = GregorianCalendar.getInstance().getTimeInMillis();
                    //dateTimeStart = System.currentTimeMillis();
                    Log.i(TAG_START_DATE, "start task");

                } else if (task.getEndDateTask() == null){

                    datetimeEnd = GregorianCalendar.getInstance().getTimeInMillis() - dateTimeStart;
                    String differenceInTime = mDifferenceTime.format(datetimeEnd);
                    task.setEndDateTask("- " + formatDateTime + ": " + differenceInTime);
                    Log.i(TAG_END_DATE, "end task");

                }

                mTaskAdapter.add(task);
                mTaskAdapter.notifyDataSetChanged();

            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                Intent longClickIntent = new Intent(MainActivity.this, TaskActivity.class);

                longClickIntent.putExtra(KEY_LONG_CLICK_ITEM, mTaskAdapter.getItem(position));
                longClickIntent.putExtra(KEY_POSITION_ITEM, position);
                Log.i(KEY_LONG_CLICK_ITEM, String.valueOf(longClickIntent.getExtras()));
                startActivityForResult(longClickIntent, 2);

                return true;
            }
        });

        mButtonAddTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //goto TaskActivity
                intent = new Intent(MainActivity.this, TaskActivity.class);
                startActivityForResult(intent, 1);
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            mTaskJSONSerializer.saveTask(mTaskArrayList);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}


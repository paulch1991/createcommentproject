package com.example.pavlo.taskproject;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.io.OutputStreamWriter;

public class TaskJSONSerializer {
    private Context mContext;
    private String mFileName;

    public TaskJSONSerializer(Context context, String fileName) {
        mContext = context;
        mFileName = fileName;
    }

    public void saveTask(ArrayList<Task> tasks) throws JSONException, IOException {
        JSONArray jsonArray = new JSONArray();
        for (Task task : tasks) {
            jsonArray.put(task.toJSON());
        }

        //JsonWriter jsonWriter = null;
        Writer writer = null;
        FileOutputStream outputStream;


        try {
            outputStream = mContext.openFileOutput(mFileName, Context.MODE_PRIVATE);

            writer = new OutputStreamWriter(outputStream);
            writer.write(jsonArray.toString());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            if(writer != null) writer.close();
        }
    }

}

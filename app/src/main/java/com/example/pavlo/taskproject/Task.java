package com.example.pavlo.taskproject;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Pasha on 29.11.2015.
 */
public class Task implements Parcelable {
    private static final String TITLE_TASK = "title_task";
    private static final String DESCRIPTION_TASK = "description_task";
    private static final String START_DATE_TASK = "start_date_task";
    private static final String END_DATE_TASK = "end_date_task";

    public String mStringTitle, mStringDescription,
            mStartDateTask, mEndDateTask;

    public Task(String stringTitle, String stringDescription) {
        this.mStringTitle = stringTitle;
        this.mStringDescription = stringDescription;
//        mStartDateTask = startDate;
//        mEndDateTask = endDate;
    }

    protected Task(Parcel in) {
        mStringTitle = in.readString();
        mStringDescription = in.readString();
        mStartDateTask = in.readString();
        mEndDateTask = in.readString();
    }

    public static final Creator<Task> CREATOR = new Creator<Task>() {
        @Override
        public Task createFromParcel(Parcel in) {
            return new Task(in);
        }

        @Override
        public Task[] newArray(int size) {
            return new Task[size];
        }
    };

    public String getStringTitle() {
        return mStringTitle;
    }

    public String getStringDescription() {
        return mStringDescription;
    }

    public void setStartDateTask(String startDateTask) {
        this.mStartDateTask = startDateTask;
    }

    public String getStartDateTask() {
        return mStartDateTask;
    }

    public void setEndDateTask(String endDateTask) {
        this.mEndDateTask = endDateTask;
    }

    public String getEndDateTask() {
        return mEndDateTask;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mStringTitle);
        dest.writeString(mStringDescription);
        dest.writeString(mStartDateTask);
        dest.writeString(mEndDateTask);
    }

    public String toJSON() throws JSONException {
        JSONObject jsonObject = new JSONObject();

        try {

            jsonObject.put(TITLE_TASK, mStringTitle);
            jsonObject.put(DESCRIPTION_TASK, mStringDescription);
            jsonObject.put(START_DATE_TASK, mStartDateTask);
            jsonObject.put(END_DATE_TASK, mEndDateTask);

            return jsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    public Task(JSONObject jsonObjects)throws JSONException{
        mStringTitle = getStringTitle();
    }
}
package com.example.pavlo.taskproject;

/**
 * Created by Pasha on 03.12.2015.
 */
public interface Constants {
    String KEY_PARCELABLE_EXTRA = "key_parcelable_extra";
    String KEY_SAVE_INSTANCE = "key_save_instance";
    String KEY_LONG_CLICK_ITEM = "key_long_click_item";
    String KEY_POSITION_ITEM = "KEY POSITION ITEM";
    String KEY_POSITION_ITEM_BACK = "KEY POSITION ITEM BACK";
    String TAG_START_DATE = "time - start doing task";
    String TAG_END_DATE = "time - done task";

    void initUI();

    void setUI();
}

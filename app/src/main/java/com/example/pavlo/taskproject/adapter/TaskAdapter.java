package com.example.pavlo.taskproject.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.pavlo.taskproject.R;
import com.example.pavlo.taskproject.Task;

import java.util.ArrayList;

/**
 * Created by Pasha on 28.11.2015.
 */
public class TaskAdapter extends ArrayAdapter<Task> {

    private ArrayList<Task> mTaskArrayList;
    ViewHolder viewHolder;
    public class ViewHolder {
        public TextView mTitleTaskTextView, mDescriptionTaskTextView, mTextViewStartDate,
                mTextViewEndDate;

        public ViewHolder(View itemView) {
            mTitleTaskTextView = (TextView) itemView.findViewById(R.id.tvTaskTitleItem);
            mDescriptionTaskTextView = (TextView) itemView.findViewById(R.id.tvTaskDescriptionItem);
            mTextViewStartDate = (TextView) itemView.findViewById(R.id.tvTaskDateStart);
            mTextViewEndDate = (TextView) itemView.findViewById(R.id.tvTaskDateEnd);
        }
    }

    public TaskAdapter(Context context, ArrayList<Task> taskArrayList) {
        super(context, 0);
        this.mTaskArrayList = taskArrayList;
    }

    @Override
    public Task getItem(int position) {
        return mTaskArrayList.get(position);
    }

    @Override
    public int getCount() {
        return mTaskArrayList.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        final Task task = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.task_item, parent, false);

            //what is this???
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.mTitleTaskTextView.setText(task.getStringTitle());
        viewHolder.mDescriptionTaskTextView.setText(task.getStringDescription());
        viewHolder.mTextViewStartDate.setText(task.getStartDateTask());
        viewHolder.mTextViewEndDate.setText(task.getEndDateTask());
        return convertView;
    }
}
